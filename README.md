# Podcast-Metadata
Save Podcast Metadata (wrapper to youtube-to-anchor)

Example yaml
```yaml
# This is a basic workflow to help you get started with Actions

name: Test Podcast-Metadata

# Controls when the workflow will run
on:
  # Triggers the workflow on push or pull request events but only for the main branch
  push:
    paths:
      - episode.json
    branches: [ main ]

  # Allows you to run this workflow manually from the Actions tab
  workflow_dispatch:

# A workflow run is made up of one or more jobs that can run sequentially or in parallel
jobs:
  # This workflow contains a single job called "build"
  build:
    # The type of runner that the job will run on
    runs-on: ubuntu-latest

    # Steps represent a sequence of tasks that will be executed as part of the job
    steps:
      # Checks-out your repository under $GITHUB_WORKSPACE, so your job can access it
      - uses: actions/checkout@v3
        with:
          token: ${{secrets.GITHUB_TOKEN}} # default token in GitHub Workflow

      - name: Upload Episode from YouTube To Anchor.Fm
        uses: abe-101/youtube-to-anchorfm@main

      - name: Save YouTube metadata in JSON db
        uses: abe-101/Podcast-Metadata@main

        env:
          ANCHOR_EMAIL: ${{ secrets.ANCHOR_EMAIL }}
          ANCHOR_PASSWORD: ${{ secrets.ANCHOR_PASSWORD }}
          EPISODE_PATH: /github/workspace
          URL_IN_DESCRIPTION: true
          SAVE_AS_DRAFT: true
          LOAD_THUMBNAIL: true

``` 


## Action to Upload Full YouTube Channel
```yaml
name: Upload Full YouTube Channel to Podcast

# Controls when the workflow will run
on:
  # Allows you to run this workflow manually from the Actions tab
  workflow_dispatch:
    inputs:
      environment:
        description: 'YouTube Channel URL'
        type: enviroment
        required: true

# A workflow run is made up of one or more jobs that can run sequentially or in parallel
jobs:
  # This workflow contains a single job called "build"
  build:
    # The type of runner that the job will run on
    runs-on: ubuntu-latest

    # Steps represent a sequence of tasks that will be executed as part of the job
    steps:
      # Checks-out your repository under $GITHUB_WORKSPACE, so your job can access it
      - uses: actions/checkout@v3
        with:
          token: ${{secrets.PERSONAL_ACCESS_TOKENS}} # default token in GitHub Workflow
      - name: Run Script
        run: |
          git config user.name "GitHub Actions Bot"
          git config user.email "<>"
          sudo apt-get install youtube-dl
          youtube-dl "${{ github.event.inputs.environment }}" --dump-json | jq '.id' -r | tac | xargs -I% bash -c "jo id='%' > episode.json && git commit -am % && git push"
```
