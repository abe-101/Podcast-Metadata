import json
import yt_dlp
import os
import csv

print('-- Adding video metadata to JSON db')
# Get YouTube ID
with open('episode.json') as file:
    episode = json.load(file)
    
# Build YouTube URL
URL = 'https://www.youtube.com/watch?v=' + episode['id']

# Retreive Metadata of YouTubr video
ydl_opts = {}
with yt_dlp.YoutubeDL(ydl_opts) as ydl:
    info = ydl.extract_info(URL, download=False)

# Build JSON object 
json_obj = json.dumps({'id': info['id'], 'published': info['upload_date'], 'title': info['title'], 'description': info['description']})
        
# add to DB
with open("episode_db.json", "a") as file:
    file.write(json_obj)
    file.write('\n')

with open("episode_db.csv", "a") as csvfile:
    fieldnames = ['Title', 'Description', 'Published', 'ID', 'URL']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    if os.stat('episode_db.csv').st_size == 0:
        writer.writeheader()
    writer.writerow({'ID': info['id'], 'Published': info['upload_date'], 'Title': info['title'], 'Description': info['description'], 'URL': URL})


os.system('git config user.name "GitHub Actions Bot"')
os.system('git config user.email "<>"')
# Commit changes
os.system(f'git add episode_db.json episode_db.csv')
# Commit message and push
os.system(f'git commit -m "db entry {info["id"]}"')
os.system(f'git pull --rebase && git push')

